all:
	gcc -W -Wall -Wextra -pedantic -g -Wcast-align -Wcast-qual -Wconversion \
	    -Wwrite-strings -Wfloat-equal -Wlogical-op -Wpointer-arith -Wformat=2 \
	    -Winit-self -Wuninitialized -Wmaybe-uninitialized -Wstrict-prototypes \
	    -Wmissing-declarations -Wmissing-prototypes -Wpadded -Wshadow -std=c99 \
	    xor.c -o xor
